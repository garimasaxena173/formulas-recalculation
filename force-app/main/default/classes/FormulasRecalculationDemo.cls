//Example in the form of Unit test
@IsTest
public with sharing class FormulasRecalculationDemo {
    
    //Note: In this case, the formula itself is:
    //Parent__r.Code__c & "|" & Code__c

    @IsTest
    static void runTest() {
	
		ParentObject__c parent = new ParentObject__c(
			Name = 'Parent',
			Code__c = 'ABAB'
		);
		INSERT parent;
		
		ChildObject__c child = new ChildObject__c(
			Name = 'Child',
			Code__c = 'XYXY',
			Parent__c = parent.Id
		);
		System.assertEquals(NULL, child.Concatenated_Formula_Field__c, 'Assert Failed');
		
		Test.startTest();
		Formula.recalculateFormulas(new List<ChildObject__c>{child});
		Test.stopTest();
		
		System.assertEquals('ABAB|XYXY', child.Concatenated_Formula_Field__c, 'Assert Failed');
	}
}
